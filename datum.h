// Copyright (c) 2012-2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#ifndef DATUM_H
#define DATUM_H

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static const int DATUM_BUF_SIZE = 1024*1024;
static const int DATUM_BUF_MIN = 32; // enough for one full precision 'double' in plaintext, i.e. '%.17lg' format

struct datum {
	int  ndim;
	int* lens;
	int  len;
	double*	data;  // 1-dimensional data access: data[0..len]
};

struct buffer {
	char* start; // original pointer
	char* then;  // before last scan
	char* now;   // after  last scan
	int scanned; // total bytes scanned
	int size;
};

void* datum_free(struct datum* dat);

struct datum* datum_alloc(const int ndim, const int* lens);

struct datum* datum_read(const char* filename);

int datum_write(const struct datum* dat, const char* filename);

int datum_index(const struct datum* dat, ...); // ... of type int

int datum_index_array(const struct datum* dat, const int* indexes);

#endif
