// Copyright (c) 2012-2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#ifndef DATUM_GZ_H
#define DATUM_GZ_H

#include <zlib.h>
#include "datum.h"

static const int DATUM_LEVEL_GZ = 9; // gzip  compression level: 9 - best, 1 - fast, 0 - none

struct datum* datum_read_gz(const char* filename);

int datum_write_gz(const struct datum* dat, const char* filename);

#endif
