// Copyright (c) 2012-2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include "datum_test_func.h"

static int datum_cmp(const struct datum* dat1, const struct datum* dat2) {
	if (dat1->len != dat2->len) {
		return 2;
	}
	for (int d = 0; d < dat1->len; d++) {
		double d1 = dat1->data[d];
		double d2 = dat2->data[d];
		if (d1 != d2) {
			if (isnan(d1) == isnan(d2)) {
				continue;
			} else if (isinf(d1) == isinf(d2)) {
				continue;
			} else {
				return 1;
			}
		}
	}
	return 0;
}

struct datum* datum_test_io_copy(const struct datum* dat, const char* filename, const char compression) {
	struct datum* dat_copy = NULL;
	if (compression == 'g') {
		datum_write_gz(dat, filename);
		dat_copy = datum_read_gz(filename);
	} else if (compression == 'b') {
		datum_write_bz2(dat, filename);
		dat_copy = datum_read_bz2(filename);
	} else {
		datum_write(dat, filename);
		dat_copy = datum_read(filename);
	}
	return dat_copy;
}

int datum_test_io(const struct datum* dat, const char* filename, const char compression) {
	printf("datum_test_io: '%s': ", filename);
	struct datum* dat_copy = datum_test_io_copy(dat, filename, compression);
	if (dat_copy == NULL) {
		return 1;
	}
	int err = datum_cmp(dat, dat_copy);
	datum_free(dat_copy);
	if (err == 0) {
		printf("pass\n");
	} else {
		printf("fail\n");
	}
	return err;
}

void datum_test_print(const struct datum* dat) {
	printf("datum_test_print:\n");
	printf("\tndim = %d\n", dat->ndim);
	printf("\tlen = %d\n", dat->len);
	for (int d = 0; d < dat->ndim; d ++) {
		printf("\tlens[%d] = %d\n", d, dat->lens[d]);
	}
	for (int d = 0; d < dat->len; d++) {
		printf("\t\tdata[%d] = %.30g\n", d, dat->data[d]);
	}
}

int datum_test_index(const struct datum* dat) {
	if (dat->ndim != 3) {
		return 1;
	}
	const int i0 = dat->lens[0] - 1;
	const int i1 = dat->lens[1] - 1;
	const int i2 = dat->lens[2] - 1;
	const int flat_index_1 = datum_index(dat, i0, i1, i2);
	const int flat_index_2 = datum_index_array(dat, (const int[]) {i0, i1, i2});
	int err = 0;
	printf("datum_test_index: ");
	if (dat->data[dat->len - 1] == dat->data[flat_index_1] &&
	    dat->data[dat->len - 1] == dat->data[flat_index_2]) {
		printf("pass\n");
	} else {
		printf("fail\n");
		err = 1;
	}
	printf("\t\tdata[%d][%d][%d] = %.30g\n", i0, i1, i2, dat->data[flat_index_1]);
	printf("\t\tdata[%d][%d][%d] = %.30g\n", i0, i1, i2, dat->data[flat_index_2]);
	return err;
}
