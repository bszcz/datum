// Copyright (c) 2012-2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#ifndef DATUM_TEST_FUNC_H
#define DATUM_TEST_FUNC_H

#include "../../datum_gz.h"
#include "../../datum_bz2.h"
#include <math.h>
#include <time.h>

int datum_test_io(const struct datum* dat, const char* filename, const char compression);

void datum_test_print(const struct datum* dat);

int datum_test_index(const struct datum* dat);

#endif
