// Copyright (c) 2012-2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <stdlib.h>

#include "datum_test_func.h"
#include "../../datum_gz.h"
#include "../../datum_bz2.h"

int main() {
	const int ndim = 3;
	const int lens[] = {2, 3, 5};
	struct datum* dat = datum_alloc(ndim, lens);
	if (dat == NULL) {
		exit(EXIT_FAILURE);
	}

	dat->data[0] = +NAN;
	dat->data[1] = -NAN;
	dat->data[2] = +INFINITY;
	dat->data[3] = -INFINITY;

	const int BYTES_IN_DOUBLE = 8;
	union {
		char c[BYTES_IN_DOUBLE];
		double x;
	} r;
	srand(time(NULL));
	for (int d = 4; d < dat->len; d++) {
		for (int b = 0; b < BYTES_IN_DOUBLE; b++) {
			r.c[b] = rand() % 256;
		}
		dat->data[d] = r.x;
	}

	datum_test_print(dat);
	int err = 0;
	err += datum_test_index(dat);
	err += datum_test_io(dat, "file.datum", 'p');
	err += datum_test_io(dat, "file.datum.gz", 'g');
	err += datum_test_io(dat, "file.datum.bz2", 'b');
	datum_free(dat);
	if (err == 0) {
		exit(EXIT_SUCCESS);
	} else {
		exit(EXIT_FAILURE);
	}
}
