// Copyright (c) 2012-2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include "datum_bz2.h"

static int datum_read_bz2_buffer(struct buffer* buf, int* iseof, BZFILE* file) {
	int err = 0;
	buf->scanned += buf->now - buf->then;
	const int buf_left = buf->size - buf->scanned;
	if (buf_left < DATUM_BUF_MIN && !(*iseof)) {
		for (int b = 0; b < buf_left; b++) {
			buf->start[b] = buf->start[b + buf->scanned]; // move unread part of the buffer
		}
		for (int b = buf_left; b < buf->size; b++) {
			buf->start[b] = '\0'; // clear the rest of the buffer
		}
		int bzerr = BZ_OK;
		if (buf->scanned != BZ2_bzRead(&bzerr, file, buf->start + buf_left, buf->scanned)) {
			if (bzerr == BZ_STREAM_END) {
				(*iseof) = 1;
			} else {
				fprintf(stderr, "datum error: BZ2_bzRead() set bzerror to '%d'\n", bzerr);
				err = 1;
			}
		}
		buf->scanned = 0;
		buf->then = buf->start; // scan from the start
	} else {
		buf->then = buf->now; // move along, scan more
	}
	return err;
}

struct datum* datum_read_bz2(const char* filename) {
	struct datum* dat = NULL;

	struct buffer buf;
	buf.size = DATUM_BUF_SIZE;
	buf.scanned = 0;
	buf.start = calloc(buf.size, sizeof(char));
	buf.then = buf.start;
	buf.now = buf.start + buf.size; // pretend all scanned at first to fill buffer
	if (buf.start == NULL) {
		fprintf(stderr, "datum error: cannot allocate memory\n");
		return NULL;
	}

	FILE* fp = fopen(filename, "rb");
	if (fp == NULL) {
		fprintf(stderr, "datum error: cannot open file '%s'\n", filename);
		goto free_buffer;
	}
	int bzerr;
	BZFILE* file;
	file = BZ2_bzReadOpen(&bzerr, fp, 0, 0, NULL, 0);
	if (file == NULL) {
		fprintf(stderr, "datum error: cannot read open BZ2 file '%s'\n", filename);
		goto fclose_file;
	}

	int iseof = 0;
	if (datum_read_bz2_buffer(&buf, &iseof, file) != 0) {
		goto fclose_file;
	}
	int ndim = strtoul(buf.then, &(buf.now), 10);
	int* lens = calloc(ndim, sizeof(int));
	if (lens == NULL) {
		fprintf(stderr, "datum error: cannot allocate memory\n");
		goto bzclose_file;
	}
	for (int d = 0; d < ndim; d++) {
		if (datum_read_bz2_buffer(&buf, &iseof, file) != 0) {
			goto free_lens;
		}
		lens[d] = strtoul(buf.then, &(buf.now), 10);
	}

	dat = datum_alloc(ndim, lens);
	if (dat == NULL) {
		goto free_lens;
	}
	for (int d = 0; d < dat->len; d++) {
		if (datum_read_bz2_buffer(&buf, &iseof, file) != 0) {
			dat = datum_free(dat);
			goto free_lens;
		}
		dat->data[d] = strtod(buf.then, &(buf.now));
	}

free_lens:
	free(lens);
bzclose_file:
	BZ2_bzReadClose(&bzerr, file);
	if (bzerr != BZ_OK) {
		fprintf(stderr, "datum error: cannot read close BZ2 file '%s'\n", filename);
	}
fclose_file:
	if (fclose(fp) != 0) {
		fprintf(stderr, "datum error: cannot close file '%s'\n", filename);
	}
free_buffer:
	free(buf.start);
	return dat;
} // datum_read_gz()

static int datum_write_bz2_bzprintf(BZFILE* bzfile, const char* fmtstr, const union anydata d) {
	char buf[DATUM_BUF_MIN];
	if (strcmp("%d\n", fmtstr) == 0) {
		snprintf(buf, DATUM_BUF_MIN, fmtstr, d._int);
	} else if (strcmp("%.17lg\n", fmtstr) == 0) {
		snprintf(buf, DATUM_BUF_MIN, fmtstr, d._double);
	}
	int bzerr = BZ_OK;
	BZ2_bzWrite(&bzerr, bzfile, buf, strlen(buf));
	return bzerr;
}

int datum_write_bz2(const struct datum* dat, const char* filename) {
	int err = 0;
	int bzerr = 0;

	FILE* file = fopen(filename, "wb");
	if (file == NULL) {
		fprintf(stderr, "datum error: cannot open file '%s'\n", filename);
		return 1;
	}
	BZFILE* bzfile = BZ2_bzWriteOpen(&bzerr, file, DATUM_LEVEL_BZ2, 0, 0); // verbosity = workFactor = 0
	if (bzfile == NULL) {
		fprintf(stderr, "datum error: cannot write open BZ2 file '%s'\n", filename);
		err = 1;
		goto fclose_file;
	}

	union anydata datapoint;
	datapoint._int = dat->ndim;
	if (datum_write_bz2_bzprintf(bzfile, "%d\n", datapoint) != BZ_OK) {
		fprintf(stderr, "datum error: cannot write to file '%s'\n", filename);
		err = 1;
		goto bzclose_file;
	}
	for (int d = 0; d < dat->ndim; d++) {
		datapoint._int = dat->lens[d];
		if (datum_write_bz2_bzprintf(bzfile, "%d\n", datapoint) != BZ_OK) {
			fprintf(stderr, "datum error: cannot write to file '%s'\n", filename);
			err = 1;
			goto bzclose_file;
		}
	}
	for (int d = 0; d < dat->len; d++) {
		datapoint._double = dat->data[d];
		if (datum_write_bz2_bzprintf(bzfile, "%.17lg\n", datapoint) != BZ_OK) {
			fprintf(stderr, "datum error: cannot write to file '%s'\n", filename);
			err = 1;
			goto fclose_file;
		}
	}

bzclose_file:
	BZ2_bzWriteClose(&bzerr, bzfile, 0, NULL, NULL); // abandon = 0, &nbytes_in = &nbytes_out = NULL
	if (bzerr != BZ_OK) {
		fprintf(stderr, "datum error: cannot write close BZ2 file '%s'\n", filename);
		err = 1;
	}
fclose_file:
	if (fclose(file) != 0) {
		fprintf(stderr, "datum error: cannot close file '%s'\n", filename);
		err = 1;
	}
	return err;
}
