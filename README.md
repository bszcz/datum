datum [![Build Status](https://travis-ci.org/bszcz/datum.svg?branch=master)](https://travis-ci.org/bszcz/datum)
-----

Simple C/Python library for reading/writing plaintext data with optional compression.


### COPYRIGHT / LICENSE

The MIT License (MIT).
