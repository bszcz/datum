// Copyright (c) 2012-2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include "datum_gz.h"

static int datum_read_gz_buffer(struct buffer* buf, int* iseof, gzFile file) {
	int err = 0;
	buf->scanned += buf->now - buf->then;
	const int buf_left = buf->size - buf->scanned;
	const int buf_left_min = 32; // enough for one full precision 'double' in plaintext, i.e. '%.17lg' format
	if (buf_left < buf_left_min && !(*iseof)) {
		for (int b = 0; b < buf_left; b++) {
			buf->start[b] = buf->start[b + buf->scanned]; // move unread part of the buffer
		}
		for (int b = buf_left; b < buf->size; b++) {
			buf->start[b] = '\0'; // clear the rest of the buffer
		}
		if (gzread(file, buf->start + buf_left, buf->scanned) != buf->scanned) {
			if (gzeof(file) == 1) {
				(*iseof) = 1;
			} else {
				fprintf(stderr, "datum error: gzerror() returned '%s'\n", gzerror(file, NULL));
				err = 1;
			}
		}
		buf->scanned = 0;
		buf->then = buf->start; // scan from the start
	} else {
		buf->then = buf->now; // move along, scan more
	}
	return err;
}

struct datum* datum_read_gz(const char* filename) {
	struct datum* dat = NULL;

	struct buffer buf;
	buf.size = DATUM_BUF_SIZE;
	buf.scanned = 0;
	buf.start = calloc(buf.size, sizeof(char));
	buf.then = buf.start;
	buf.now = buf.start + buf.size; // pretend all scanned at first to fill buffer
	if (buf.start == NULL) {
		fprintf(stderr, "datum error: cannot allocate memory\n");
		return NULL;
	}

	gzFile file;
	file = gzopen(filename, "rb");
	if (file == NULL) {
		fprintf(stderr, "datum error: cannot open file '%s'\n", filename);
		goto free_buffer;
	}

	int iseof = 0;
	if (datum_read_gz_buffer(&buf, &iseof, file) != 0) {
		goto gzclose_file;
	}
	int ndim = strtoul(buf.then, &(buf.now), 10);
	int* lens = calloc(ndim, sizeof(int));
	if (lens == NULL) {
		fprintf(stderr, "datum error: cannot allocate memory\n");
		goto gzclose_file;
	}
	for (int d = 0; d < ndim; d++) {
		if (datum_read_gz_buffer(&buf, &iseof, file) != 0) {
			goto free_lens;
		}
		lens[d] = strtoul(buf.then, &(buf.now), 10);
	}

	dat = datum_alloc(ndim, lens);
	if (dat == NULL) {
		goto free_lens;
	}
	for (int d = 0; d < dat->len; d++) {
		if (datum_read_gz_buffer(&buf, &iseof, file) != 0) {
			dat = datum_free(dat);
			goto free_lens;
		}
		dat->data[d] = strtod(buf.then, &(buf.now));
	}

free_lens:
	free(lens);
gzclose_file:
	if (gzclose(file) != Z_OK) {
		fprintf(stderr, "datum error: cannot close file '%s'\n", filename);
	}
free_buffer:
	free(buf.start);
	return dat;
} // datum_read_gz()

int datum_write_gz(const struct datum* dat, const char* filename) {
	int err = 0;
	char gzmode[4];
	sprintf(gzmode, "wb%d", DATUM_LEVEL_GZ);
	gzFile file = gzopen(filename, gzmode);
	if (file == NULL) {
		fprintf(stderr, "datum error: cannot open file '%s'\n", filename);
		return 1;
	}
	if (gzprintf(file, "%d\n", dat->ndim) < 1) {
		fprintf(stderr, "datum error: cannot write to file '%s'\n", filename);
		err = 1;
		goto gzclose_file;
	}
	for (int d = 0; d < dat->ndim; d++) {
		if (gzprintf(file, "%d\n", dat->lens[d]) < 1) {
			fprintf(stderr, "datum error: cannot write to file '%s'\n", filename);
			err = 1;
			goto gzclose_file;
		}
	}
	for (int d = 0; d < dat->len; d++) {
		if (gzprintf(file, "%.17lg\n", dat->data[d]) < 1) {
			fprintf(stderr, "datum error: cannot write to file '%s'\n", filename);
			err = 1;
			goto gzclose_file;
		}
	}

gzclose_file:
	if (gzclose(file) != Z_OK) {
		fprintf(stderr, "datum error: cannot close file '%s'\n", filename);
		err = 1;
	}
	return err;
} // datum_write_gz()
