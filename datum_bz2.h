// Copyright (c) 2012-2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#ifndef DATUM_BZ2_H
#define DATUM_BZ2_H

#include <bzlib.h>
#include "datum.h"

static const int DATUM_LEVEL_BZ2 = 9; // bzip2 compression level: 9 - best, 1 - fast

union anydata {
	double _double;
	int _int;
};

struct datum* datum_read_bz2(const char* filename);

int datum_write_bz2(const struct datum* dat, const char* filename);

#endif
