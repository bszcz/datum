# Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
# This program is free software under the MIT license.

import numpy

class Datum:
	def __init__(self, x):
		if isinstance(x, str):
			self.read(x)
		elif isinstance(x, list):
			self.calloc(x)
		else:
			raise ValueError("datum error: wrong initialisation parameter")

	def calloc(self, lens):
		if len(lens) < 1:
			raise ValueError("datum error: number of dimensions = %d < 1" % len(lens))
		for i in range(len(lens)):
			if lens[i] < 1:
				raise ValueError("datum error: dimension[%d] = %d < 1" % (i, lens[i]))
		self.data = numpy.zeros(lens)

	def read(self, filename):
		self.data = numpy.loadtxt(filename)
		ndim = self.data[0]
		lens = self.data[1 : ndim + 1]
		self.data = self.data[ndim + 1 : ]
		self.data = self.data.reshape(lens)

	def write(self, filename):
		lens = self.data.shape
		filedata = numpy.append([len(lens)], lens)
		filedata = numpy.append(filedata, self.data.ravel())
		numpy.savetxt(filename, filedata, fmt = "%.17g")
