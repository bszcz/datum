// Copyright (c) 2012-2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include "datum.h"

void* datum_free(struct datum* dat) {
	if (dat == NULL) {
		fprintf(stderr, "datum error: datum pointer = NULL\n");
	} else {
		free(dat->lens);
		free(dat->data);
		free(dat);
	}
	return NULL;
}

static void* datum_alloc_err(struct datum* dat) {
	fprintf(stderr, "datum error: cannot allocate memory\n");
	datum_free(dat);
	return NULL;
}

struct datum* datum_alloc(const int ndim, const int* lens) {
	if (ndim < 1) {
		fprintf(stderr, "datum error: number of dimensions = %d < 1\n", ndim);
		return NULL;
	}
	for (int d = 0; d < ndim; d++) {
		if (lens[d] < 1) {
			fprintf(stderr, "datum error: dimension[%d] = %d < 1\n", d, lens[d]);
			return NULL;
		}
	}

	struct datum* dat = calloc(1, sizeof(struct datum));
	if (dat == NULL) {
		return datum_alloc_err(dat);
	}
	dat->ndim = ndim;
	dat->data = NULL;  // for clean freeing

	dat->lens = calloc(ndim, sizeof(int));
	if (dat->lens == NULL) {
		return datum_alloc_err(dat);
	}
	dat->len = 1;
	for (int d = 0; d < ndim; d++) {
		dat->lens[d] = lens[d];
		dat->len *= lens[d];
	}

	dat->data = calloc(dat->len, sizeof(double));
	if (dat->data == NULL) {
		return datum_alloc_err(dat);
	}

	return dat;
} // datum_alloc()

struct datum* datum_read(const char* filename) {
	struct datum* dat = NULL;

	FILE* file = fopen(filename, "rb");
	if (file == NULL) {
		fprintf(stderr, "datum error: cannot open file '%s'\n", filename);
		return NULL;
	}
	int ndim;
	if (fscanf(file, "%d", &ndim) < 0) {
		fprintf(stderr, "datum error: cannot read dimension number\n");
		goto fclose_file;
	}
	int* lens = calloc(ndim, sizeof(int));
	if (lens == NULL) {
		fprintf(stderr, "datum error: cannot allocate memory\n");
		goto fclose_file;
	}
	for (int d = 0; d < ndim; d++) {
		if (fscanf(file, "%d", &lens[d]) < 1) {
			fprintf(stderr, "datum error: cannot read dimension %d size\n", d);
			goto free_lens;
		}
	}

	dat = datum_alloc(ndim, lens);
	if (dat == NULL) {
		goto free_lens;
	}
	for (int d = 0; d < dat->len; d++) {
		if (fscanf(file, "%lf", &dat->data[d]) < 1) {
			fprintf(stderr, "datum error: cannot read data point %d\n", d);
			dat = datum_free(dat);
			goto free_lens;
		}
	}

free_lens:
	free(lens);
fclose_file:
	if (fclose(file) != 0) {
		fprintf(stderr, "datum error: cannot close file '%s'\n", filename);
		return NULL;
	}
	return dat;
} // datum_read()

int datum_write(const struct datum* dat, const char* filename) {
	int err = 0;

	FILE* file = fopen(filename, "wb");
	if (file == NULL) {
		fprintf(stderr, "datum error: cannot open file '%s'\n", filename);
		return 1;
	}
	if (fprintf(file, "%d\n", dat->ndim) < 1) {
		fprintf(stderr, "datum error: cannot write to file '%s'\n", filename);
		err = 1;
		goto fclose_file;
	}
	for (int d = 0; d < dat->ndim; d++) {
		if (fprintf(file, "%d\n", dat->lens[d]) < 1) {
			fprintf(stderr, "datum error: cannot write to file '%s'\n", filename);
			err = 1;
			goto fclose_file;
		}
	}
	for (int d = 0; d < dat->len; d++) {
		if (fprintf(file, "%.17lg\n", dat->data[d]) < 1) {
			fprintf(stderr, "datum error: cannot write to file '%s'\n", filename);
			err = 1;
			goto fclose_file;
		}
	}

fclose_file:
	if (fclose(file) != 0) {
		fprintf(stderr, "datum error: cannot close file '%s'\n", filename);
		err = 1;
	}
	return err;
} // datum_write()

inline
int datum_index(const struct datum* dat, ...) {
	va_list indexes;
	va_start(indexes, dat);
	int index = 0;
	int flat_index = 0;
	for (int i = 1; i < dat->ndim; i++) {
		index = va_arg(indexes, int);
		flat_index += index;
		flat_index *= dat->lens[i];
	}
	index = va_arg(indexes, int);
	flat_index += index;
	va_end(indexes);
	return flat_index;
}

inline
int datum_index_array(const struct datum* dat, const int* indexes) {
	int flat_index = 0;
	for (int i = 0; i < dat->ndim - 1; i++) {
		flat_index += indexes[i];
		flat_index *= dat->lens[i + 1];
	}
	flat_index += indexes[dat->ndim - 1];
	return flat_index;
}
